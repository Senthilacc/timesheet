package ca.rsagroup.cgn.mappings;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.transaction.Transactional;

/**
 * Created by t900435 on 3/18/2015.
 */
public class TestHibernateMappings {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "classpath*:**/persistence-config.xml");

       Object obj = context.getBean("mySessionFactory");
        SessionFactory factory = (SessionFactory)obj;

        Session session = factory.openSession();
        session.beginTransaction();
        factory.getCurrentSession().createQuery("FROM Employee ORDER BY id");
        System.out.println( obj!=null );
    }
}
