package ca.rsagroup.cgn.mappings;

import ca.rsagroup.cgn.dto.TimesheetDetailsDTO;
import ca.rsagroup.cgn.model.Employee;
import ca.rsagroup.cgn.model.Timesheet;
import ca.rsagroup.cgn.model.Task;
import ca.rsagroup.cgn.model.TimesheetDetail;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by t900435 on 3/18/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:**/persistence-config.xml"})
public class TestHibernate extends AbstractJUnit4SpringContextTests {

    @Autowired
    private ApplicationContext context;

   // @Test
    public void test() {
//       Object obj= context.getBean("myDataSource");
        for(String name: context.getBeanDefinitionNames()){
            System.out.println(name);
        }
        SessionFactory factory = (SessionFactory)context.getBean("mySessionFactory");
        Session session = factory.openSession();
//        List list1 = session.getNamedQuery("getTimeSheetDetails").list();
        Query query = session.getNamedQuery("getTimeSheetDetails");
        query.setParameter("date",getDate(11,02,2015));
        query.setResultTransformer(Transformers.aliasToBean(TimesheetDetailsDTO.class));
        List list1 = query.list();
        System.out.println(list1.size());
//        List<TimesheetDetail> list = (List<TimesheetDetail>)session.
//                createQuery("FROM TimesheetDetail ORDER BY id").list();
//        System.out.println( list.get(0).getEmployee().getFirstname() );
//        System.out.println( list.get(0).getEmployee().getLastname() );
//        System.out.println(list.get(0).getHours() );
//        System.out.println( list.get(0).getTask().getTaskName() );
//        System.out.println( list.get(0).getTimesheet().getId() );
//        System.out.println( list.get(0).getTimesheet().getStartDate());
//        System.out.println( list.get(0).getTimesheet().getEndDate() );
//        System.out.println( list.get(0).getWorkDate() );
//        System.out.println( list.get(0).getTask().getTaskName() );

    }
  // @Test
    public void testInsert(){
        SessionFactory factory = (SessionFactory)context.getBean("mySessionFactory");
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        TimesheetDetail timeSheetDetail = new TimesheetDetail();
        timeSheetDetail.setWorkDate(new Date());
        timeSheetDetail.setHours(new BigDecimal(50l));
        Task task = new Task();
        task.setTaskName("SDkfasdkf");
        task = loadOrInsert(session,task);
        List<Timesheet> timesheetList = session.createQuery("from Timesheet t where t.id = :id").setInteger("id",1).list();

        Employee employee = new Employee();
        employee.setFirstname("Senthil");
        employee.setLastname("Chinraj");
        employee = loadOrInsert(session, employee);

        timeSheetDetail.setEmployee( employee );
        timeSheetDetail.setTask( task );
        timeSheetDetail.setTimesheet( timesheetList.get(0));
        session.saveOrUpdate(timeSheetDetail);
        tx.commit();
        session.close();
    }

    private Task loadOrInsert(Session session, Task task){
        List<Task> taskList = session.createQuery("from Task task where task.taskName = :TaskName ").setString("TaskName", task.getTaskName()).list();
        if(taskList.isEmpty()){
            session.save( task );
            return task;
        }
        return taskList.get(0);
    }

    private Employee loadOrInsert(Session session, Employee employee){
        List<Employee> employees = session.createQuery("from Employee emp where emp.firstname = :firstName and emp.lastname = :lastName ").setString("firstName", employee.getFirstname()).setString("lastName",employee.getLastname()).list();
        if(employees.isEmpty()){
            employee.setProjectRole("Senior Software Developer");
            session.save( employee );
            return employee;
        }
        return employees.get(0);
    }

//    @Test
    public void testReturnTimesheet(){
        SessionFactory factory = (SessionFactory)context.getBean("mySessionFactory");
        Session session = factory.openSession();
        Timesheet timesheet = (Timesheet)session.createQuery("From Timesheet tm where tm.isActive=true ").uniqueResult();
        System.out.println(timesheet!=null);
    }

    @Test
    public void getAll() {
        SessionFactory factory = (SessionFactory)context.getBean("mySessionFactory");
        Session session = factory.openSession();
        Criteria criteria = session.createCriteria(TimesheetDetail.class);
        criteria.createAlias("employee", "emp");
        criteria.createAlias("timesheet", "tm");
        criteria.add(Restrictions.eq("emp.firstname", "Senthil"));
        criteria.add(Restrictions.eq("emp.lastname", "Chinraj"));
        criteria.add(Restrictions.eq("tm.id", 1l));
        List<TimesheetDetail> list = criteria.list();
        for(TimesheetDetail tm: list){
            session.delete( tm );
        }
        session.flush();
//        System.out.print(list.size());
    }

    private Date getDate(int day, int month, int year){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);
        return cal.getTime();
    }
}
