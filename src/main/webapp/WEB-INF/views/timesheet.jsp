<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@
	taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%@
	taglib prefix="spring" uri="http://www.springframework.org/tags"
%><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Timesheet</title>
		<link rel="stylesheet" href="<c:url value='/styles/default.css'/>">
	</head>
    <script type="text/javascript">
        function validate(){
            var value = document.getElementById('fileName').value;
            if( value==null || value.indexOf('.csv')==-1){
                alert("Invalid file selected for upload");
            }else{
                document.getElementById('form1').submit();
            }
        }
    </script>
	<body>
		<h1>Upload TimeSheet</h1>


        <form:form id="form1" method="POST" action="uploadFile" enctype="multipart/form-data">
            </br>
            <span style="font-weight: bold;">File to upload(CSV): </span><input id="fileName" type="file" name="file">&nbsp;&nbsp;<input type="submit" onclick="validate();return false;" value="Click me To Upload"/>
            <c:if test="${param.result == 'success' }" >
                <span style="color:lightgreen;">File has been successfully Submitted.</span>
            </c:if>
            <c:if test="${param.result == 'failed' }">
                <span style="color:red">Error Occured!! Please try again.</span>
            </c:if>
        </form:form>

        </br>
        </br>
        </br>
        </br>
        </br>

        <h2> Submitted Timesheet Entries</h2>

			<table>
				<thead><tr><th>Name</th><th>Time Period</th><th>Hours</th><th>Submitted Timestamp</th></tr></thead>
				<tbody>
					<c:forEach var="userEntry" items="${entries}" varStatus="row">
						<tr>
							<td><span>${userEntry.employeeName}</span></td>
                            <td> <span>${userEntry.timeSheetPeriod} </span> </td>
                            <td> <span>${userEntry.hours}</span></td>
                            <td> <span>${userEntry.createdTimestamp}</span></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
        <div align="center">
            <h3><a href="/FinalTimeReport">Download Consolidated Report</a></h3>
        </div>
		<script src="<c:url value='/scripts/lib/prototype.js'/>"></script>
		<script src="<c:url value='/scripts/lib/RowSelector.js'/>"></script>
		<script src="<c:url value='/scripts/user.js'/>"></script>
	</body>
</html>
