package ca.rsagroup.cgn.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by t900435 on 3/18/2015.
 */
@Entity
@Table(name="task")
public class Task implements Serializable {

    private static final long serialVersionUID = -3901783801585381299L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(name="task_name")
    private String taskName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }
}


