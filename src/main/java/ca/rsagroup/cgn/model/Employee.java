package ca.rsagroup.cgn.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by t900435 on 3/17/2015.
 */
@Entity
@Table(name="employee")
public class Employee implements Serializable {


    private static final long serialVersionUID = -1043458951326015858L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "first_name")
    @NotNull
    private String firstname;

    @Column(name= "last_name")
    private String lastname;

    @Column(name="project_role")
    @NotNull
    private String projectRole;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getProjectRole() {
        return projectRole;
    }

    public void setProjectRole(String projectRole) {
        this.projectRole = projectRole;
    }
}
