package ca.rsagroup.cgn.service.impl;

import ca.rsagroup.cgn.dao.TimeSheetDAO;
import ca.rsagroup.cgn.dto.TimesheetDetailsDTO;
import ca.rsagroup.cgn.model.Timesheet;
import ca.rsagroup.cgn.service.TimeSheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by t900435 on 3/22/2015.
 */
@Service
public class TimeSheetServiceImpl implements TimeSheetService {

    @Autowired
    private TimeSheetDAO dao;

    @Override
    @Transactional(readOnly = true)
    public Timesheet getActiveTimesheet() {
        return dao.getActiveTimesheet();
    }

    @Override
    @Transactional(readOnly = true)
    public List<TimesheetDetailsDTO> getAllTimeSheetEntries() {
        return dao.getTimeSheetDetailEntries();
    }
}
