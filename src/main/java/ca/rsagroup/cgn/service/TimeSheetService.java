package ca.rsagroup.cgn.service;

import ca.rsagroup.cgn.dto.TimesheetDetailsDTO;
import ca.rsagroup.cgn.model.Timesheet;

import java.util.List;

/**
 * Created by t900435 on 3/22/2015.
 */
public interface TimeSheetService {

    public Timesheet getActiveTimesheet();

    public List<TimesheetDetailsDTO> getAllTimeSheetEntries();

}
