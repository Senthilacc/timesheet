package ca.rsagroup.cgn.excel;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by t900435 on 3/22/2015.
 */
public class TimeSheetDetailRow {

    private Long employeeId;

    private String employeeName;

    private String employeeRole;

    private String location;

    private String rate;

    private Long taskId;

    private String projectId;

    private String taskName;

    private Map<String, BigDecimal> hoursMap = new HashMap<String,BigDecimal>();

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeRole() {
        return employeeRole;
    }

    public void setEmployeeRole(String employeeRole) {
        this.employeeRole = employeeRole;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Map<String, BigDecimal> getHoursMap() {
        return hoursMap;
    }

    public void setHoursMap(Map<String, BigDecimal> hoursMap) {
        this.hoursMap = hoursMap;
    }

    public void put(String workDate, BigDecimal hours){
        getHoursMap().put( workDate, hours );
    }

    public BigDecimal getHours(String workDate){
        return getHoursMap().get( workDate );
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
}
