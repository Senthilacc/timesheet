package ca.rsagroup.cgn.excel;

import org.jsefa.csv.annotation.CsvDataType;
import org.jsefa.csv.annotation.CsvField;

/**
 * Created by t900435 on 3/19/2015.
 */
@CsvDataType
public class CSVToPOJO {

    private final static int dateStartPosition = 3;

    @CsvField(pos=1)
    private String resourceName;

    @CsvField(pos=2)
    private String projectActivity;

    @CsvField(pos=CSVToPOJO.dateStartPosition)
    private String date1;

    @CsvField(pos=CSVToPOJO.dateStartPosition+1)
    private String date2;

    @CsvField(pos=CSVToPOJO.dateStartPosition+2)
    private String date3;

    @CsvField(pos=CSVToPOJO.dateStartPosition+3)
    private String date4;

    @CsvField(pos=CSVToPOJO.dateStartPosition+4)
    private String date5;

    @CsvField(pos=CSVToPOJO.dateStartPosition+5)
    private String date6;

    @CsvField(pos=CSVToPOJO.dateStartPosition+6)
    private String date7;

    public static int getDateStartPosition() {
        return dateStartPosition;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getProjectActivity() {
        return projectActivity;
    }

    public void setProjectActivity(String projectActivity) {
        this.projectActivity = projectActivity;
    }

    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        this.date1 = date1;
    }

    public String getDate2() {
        return date2;
    }

    public void setDate2(String date2) {
        this.date2 = date2;
    }

    public String getDate3() {
        return date3;
    }

    public void setDate3(String date3) {
        this.date3 = date3;
    }

    public String getDate4() {
        return date4;
    }

    public void setDate4(String date4) {
        this.date4 = date4;
    }

    public String getDate5() {
        return date5;
    }

    public void setDate5(String date5) {
        this.date5 = date5;
    }

    public String getDate6() {
        return date6;
    }

    public void setDate6(String date6) {
        this.date6 = date6;
    }

    public String getDate7() {
        return date7;
    }

    public void setDate7(String date7) {
        this.date7 = date7;
    }
}
