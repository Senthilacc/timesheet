package ca.rsagroup.cgn.excel;

import org.jsefa.csv.annotation.CsvDataType;
import org.jsefa.csv.annotation.CsvField;

/**
 * Created by t900435 on 3/19/2015.
 */
@CsvDataType
public class CSVToTaskPojo {

    @CsvField(pos=10)
    private String taskName;

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }
}
