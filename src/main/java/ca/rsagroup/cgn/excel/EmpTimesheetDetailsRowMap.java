package ca.rsagroup.cgn.excel;

import ca.rsagroup.cgn.dto.TimesheetDetailsDTO;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by t900435 on 3/22/2015.
 */
public class EmpTimesheetDetailsRowMap {

    private Long employeeId;

    private Map<Long,TimeSheetDetailRow> timeSheetDetailRowMap = new HashMap<Long,TimeSheetDetailRow>();

    public TimeSheetDetailRow getTimeSheetDetailRow(Long taskId, TimesheetDetailsDTO timesheetDetailsDTO){
        TimeSheetDetailRow timeSheetDetailRow = timeSheetDetailRowMap.get(taskId);
        if(timeSheetDetailRow==null){
            timeSheetDetailRow = mapTimeSheetDetailAttributes(timesheetDetailsDTO);
            timeSheetDetailRowMap.put(taskId, timeSheetDetailRow );
        }
        return timeSheetDetailRow;
    }

    private TimeSheetDetailRow mapTimeSheetDetailAttributes( TimesheetDetailsDTO timesheetDetailsDTO ){
        TimeSheetDetailRow row = new TimeSheetDetailRow();
        row.setTaskName( timesheetDetailsDTO.getTaskName() );
        row.setEmployeeId( timesheetDetailsDTO.getEmployeeId() );
        row.setEmployeeName( timesheetDetailsDTO.getEmployeeName() );
        row.setEmployeeRole( timesheetDetailsDTO.getEmployeeRole() );
        row.setLocation( timesheetDetailsDTO.getLocation() );
        row.setProjectId( timesheetDetailsDTO.getProjectId() );
        row.setTaskId( timesheetDetailsDTO.getTaskId() );

        return row;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Map<Long, TimeSheetDetailRow> getTimeSheetDetailRowMap() {
        return timeSheetDetailRowMap;
    }

    public void setTimeSheetDetailRowMap(Map<Long, TimeSheetDetailRow> timeSheetDetailRowMap) {
        this.timeSheetDetailRowMap = timeSheetDetailRowMap;
    }
}
