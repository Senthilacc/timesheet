package ca.rsagroup.cgn.web;

import ca.rsagroup.cgn.service.TimeSheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by t900435 on 3/22/2015.
 */
@Controller
public class DownloadConroller {

    @Autowired
    private TimeSheetService service;

    @RequestMapping(value = "/FinalTimeReport", method = RequestMethod.GET)
    public ModelAndView downloadExcel() {
        ModelAndView modelAndView = new ModelAndView("excelView");
        modelAndView.addObject( "list", service.getAllTimeSheetEntries());
        modelAndView.addObject( "tm", service.getActiveTimesheet() );
        return modelAndView;
    }

}
