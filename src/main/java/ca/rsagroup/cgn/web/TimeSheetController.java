package ca.rsagroup.cgn.web;

import ca.rsagroup.cgn.service.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/timesheet")
public class TimeSheetController {

	static final String URL = "/timesheet";

    @Autowired
    private FileUploadService fileUploadService;

	@RequestMapping(method = RequestMethod.GET)
	public void get(Model model) {
        model.addAttribute("entries",fileUploadService.getSubmittedEntries());
	}
	
//	@RequestMapping(method = RequestMethod.POST)
//	public String post(Model model, @Valid UserCommand userCommand, BindingResult result) {
//		if (result.hasErrors()) {
//			model.addAttribute("userGrid", userService.findAll());
//			return URL;
//		}
//		PasswordEncoder passwordEncoder = new StandardPasswordEncoder();
//		userCommand.setPassword(passwordEncoder.encode(userCommand.getPassword()));
//		userService.save(userCommand);
//
//		return "redirect:" + URL;
//	}
	
//	@RequestMapping(method = RequestMethod.POST, params="_method=put")
//	public String put(Model model, @Valid UserGrid userGrid, BindingResult result) {
//		if (result.hasErrors()) {
//			userService.updateWithAll(userGrid);
//			return URL;
//		}
//		userService.saveAll(userGrid);
//		return "redirect:" + URL;
//	}
	
}
