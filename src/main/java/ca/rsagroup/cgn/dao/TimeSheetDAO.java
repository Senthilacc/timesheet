package ca.rsagroup.cgn.dao;

import ca.rsagroup.cgn.model.Employee;
import ca.rsagroup.cgn.dto.SubmittedEntriesDTO;
import ca.rsagroup.cgn.dto.TimesheetDetailsDTO;
import ca.rsagroup.cgn.model.Task;
import ca.rsagroup.cgn.model.Timesheet;
import ca.rsagroup.cgn.model.TimesheetDetail;

import java.util.List;

/**
 * Created by t900435 on 3/19/2015.
 */
public interface TimeSheetDAO {

    public Timesheet getActiveTimesheet();

    public Employee saveOrLoadEmployee(Employee employee);

    public Task saveOrLoadTask(Task task);

    public void saveOrUpdate(TimesheetDetail timesheetDetail);

    public void deleteTimesheet(Employee employee, Long timesheetId);

    public List<SubmittedEntriesDTO> getSubmittedEntries();

    public List<TimesheetDetailsDTO> getTimeSheetDetailEntries();

}
