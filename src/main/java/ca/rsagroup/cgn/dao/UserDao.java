package ca.rsagroup.cgn.dao;

import java.util.List;

import ca.rsagroup.cgn.model.User;

public interface UserDao {

	User get(Long id);
	void save(User user);
	void delete(User user);
	List<User> findAll();
	User findByUserName(String username);

}
