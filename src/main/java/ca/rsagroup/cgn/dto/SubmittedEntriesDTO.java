package ca.rsagroup.cgn.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by t900435 on 3/22/2015.
 */
public class SubmittedEntriesDTO {

    private String employeeName;

    private String timeSheetPeriod;

    private BigDecimal hours;

    private Date createdTimestamp;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getTimeSheetPeriod() {
        return timeSheetPeriod;
    }

    public void setTimeSheetPeriod(String timeSheetPeriod) {
        this.timeSheetPeriod = timeSheetPeriod;
    }

    public BigDecimal getHours() {
        return hours;
    }

    public void setHours(BigDecimal hours) {
        this.hours = hours;
    }

    public Date getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(Date createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }
}
