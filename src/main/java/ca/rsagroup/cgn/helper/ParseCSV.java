package ca.rsagroup.cgn.helper;

import ca.rsagroup.cgn.model.Employee;
import ca.rsagroup.cgn.excel.CSVToPOJO;
import ca.rsagroup.cgn.model.Task;
import ca.rsagroup.cgn.model.TimesheetDetail;
import org.jsefa.Deserializer;
import org.jsefa.csv.CsvIOFactory;
import org.jsefa.csv.config.CsvConfiguration;
import org.springframework.stereotype.Component;

import java.io.Reader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by t900435 on 3/21/2015.
 */
@Component("parseCSV")
public class ParseCSV {

    public List<TimesheetDetail> parseCSV( Reader reader ){
        CsvConfiguration config = new CsvConfiguration();
        config.setFieldDelimiter(',');
        Deserializer deserializer = CsvIOFactory.createFactory(config, CSVToPOJO.class).createDeserializer();
        deserializer.open( reader);
        CSVToPOJO headerObj = null;
        List<TimesheetDetail> timesheetDetailsList = new ArrayList<TimesheetDetail>();
        while(deserializer.hasNext()){
            if(headerObj==null) {
                headerObj = deserializer.next();
            }else{
                CSVToPOJO pojo = deserializer.next();
                parseTimesheetdetails(pojo, headerObj, timesheetDetailsList);
            }

        }
        return timesheetDetailsList;
    }

    private void  parseTimesheetdetails(CSVToPOJO pojo, CSVToPOJO headerObj, List<TimesheetDetail> detailList){
        if( pojo.getDate1() !=null && pojo.getDate1().trim()!=null  ){
            detailList.add(mapTimeSheetDetail(pojo,headerObj.getDate1(),pojo.getDate1()));
        }
        if( pojo.getDate2() !=null && pojo.getDate2().trim()!=null  ){
            detailList.add(mapTimeSheetDetail(pojo,headerObj.getDate2(),pojo.getDate2()));
        }
        if( pojo.getDate3() !=null && pojo.getDate3().trim()!=null  ){
            detailList.add(mapTimeSheetDetail(pojo,headerObj.getDate3(),pojo.getDate3()));
        }
        if( pojo.getDate4() !=null && pojo.getDate4().trim()!=null  ){
            detailList.add(mapTimeSheetDetail(pojo,headerObj.getDate4(),pojo.getDate4()));
        }
        if( pojo.getDate5() !=null && pojo.getDate5().trim()!=null  ){
            detailList.add(mapTimeSheetDetail(pojo,headerObj.getDate5(),pojo.getDate5()));
        }
        if( pojo.getDate6() !=null && pojo.getDate6().trim()!=null  ){
            detailList.add(mapTimeSheetDetail(pojo,headerObj.getDate6(),pojo.getDate6()));
        }
        if( pojo.getDate7() !=null && pojo.getDate7().trim()!=null  ){
            detailList.add(mapTimeSheetDetail(pojo,headerObj.getDate7(),pojo.getDate7()));
        }
    }

    private TimesheetDetail mapTimeSheetDetail(CSVToPOJO pojo, String date, String hours){
        TimesheetDetail detail = new TimesheetDetail();
        detail.setTask( mapTaskName( pojo) );
        detail.setEmployee( mapEmployeeDetails(pojo));
        detail.setWorkDate( getDate(date) );
        detail.setHours( BigDecimal.valueOf(Long.valueOf(hours)) );
        return detail;
    }

    private Employee mapEmployeeDetails(CSVToPOJO pojo){
        Employee employee = new Employee();
        String[] name = pojo.getResourceName().split(" ");
        employee.setFirstname( name[0] );
        employee.setLastname( name[1] );
        return employee;
    }

    private Task mapTaskName(CSVToPOJO pojo){
        Task task = new Task();
        task.setTaskName( pojo.getProjectActivity() );
        return task;
    }

    private Date getDate(String date){
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
        try {
            return format.parse( date );
        } catch (ParseException e) {
             e.printStackTrace();
            return null;
        }
    }
}
